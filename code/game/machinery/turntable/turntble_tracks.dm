/datum/turntable_soundtrack/ragtime
	f_name = "R"
	name = "agtime Piano"
	path = 'sound/turntable/TheEntertainer.ogg'

/*
/datum/turntable_soundtrack/assistantblues
	f_name = "L"
	name = "onely Assistant Blues"
	path = 'sound/turntable/AGrainOfSandInSandwich.ogg'


/datum/turntable_soundtrack/chinatown
	f_name = "C"
	name = "hinatown"
	path = 'sound/turntable/chinatown.ogg'


/datum/turntable_soundtrack/bluetheme
	f_name = "B"
	name = "lue Theme"
	path = 'sound/turntable/BlueTheme.ogg'


/datum/turntable_soundtrack/jessesjames
	f_name = "T"
	name = "he Assassination of Jesse James"
	path = 'sound/turntable/TheAssassinationOfJesseJames.ogg'


/datum/turntable_soundtrack/vices
	f_name = "E"
	name = "veryone Has Their Vices"
	path = 'sound/turntable/EveryoneHasTheirVices.ogg'


/datum/turntable_soundtrack/theway
	f_name = "T"
	name = "he Way You Look Tonight"
	path = 'sound/turntable/TheWayYouLookTonight.ogg'


/datum/turntable_soundtrack/onizukas
	f_name = "O"
	name = "nizukas Blues"
	path = 'sound/turntable/OnizukasBlues.ogg'


/datum/turntable_soundtrack/ithadtobe
	f_name = "I"
	name = "t Had To Be You"
	path = 'sound/turntable/ItHadToBeYou.ogg'


/datum/turntable_soundtrack/dayswork
	f_name = "A"
	name = "nother Day's Work"
	path = 'sound/turntable/AnotherDaysWork.ogg'


/datum/turntable_soundtrack/mutebeat
	f_name = "M"
	name = "ute Beat"
	path = 'sound/turntable/MuteBeat.ogg'


/datum/turntable_soundtrack/groovy
	f_name = "G"
	name = "roovy Times"
	path = 'sound/turntable/GroovyTime.ogg'


/datum/turntable_soundtrack/thatsall
	f_name = "T"
	name = "hat`s All"
	path = 'sound/turntable/ThatsAll.ogg'


/datum/turntable_soundtrack/bluejazz
	f_name = "C"
	name = "lassic Blue Jazz"
	path = 'sound/turntable/classicbluejazz.ogg'


/datum/turntable_soundtrack/homosex
	f_name = "H"
	name = "omosexualism"
	path = 'sound/turntable/anthem.ogg'


/datum/turntable_soundtrack/comebackdown
	f_name = "C"
	name = "ome Back Down"
	path = 'sound/turntable/comebackdown.ogg'


/datum/turntable_soundtrack/leperv
	f_name = "L"
	name = "e Perv"
	path = 'sound/turntable/leperv.ogg'


/datum/turntable_soundtrack/parasite
	f_name = "P"
	name = "arasite"
	path = 'sound/turntable/parasite.ogg'


/datum/turntable_soundtrack/teenwerewolf
	f_name = "I"
	name = " Was a Teenage Werewolf"
	path = 'sound/turntable/IWasATeenageWerewolf.ogg'


/datum/turntable_soundtrack/jackripper
	f_name = "J"
	name = "ack The Ripper"
	path = 'sound/turntable/JackTheRipper.ogg'


/datum/turntable_soundtrack/discock
	f_name = "L"
	name = "ine Disaster Chicken"
	path = 'sound/turntable/LineDisasterChicken.ogg'

/datum/turntable_soundtrack/massacre
	f_name = "M"
	name = "assacre Anemone"
	path = 'sound/turntable/MassacreAnemone.ogg'

/datum/turntable_soundtrack/discock
	f_name = "T"
	name = "he Way I Walk"
	path = 'sound/turntable/TheWayIWalk.ogg'

/datum/turntable_soundtrack/youngmen
	f_name = "Y"
	name = "oung Men Dead"
	path = 'sound/turntable/YoungMenDead.ogg'

/datum/turntable_soundtrack/ontherun
	f_name = "Y"
	name = "ou On The Run"
	path = 'sound/turntable/YouOnTheRun.ogg'

/datum/turntable_soundtrack/anarchy
	f_name = "A"
	name = "narchy Road"
	path = 'sound/turntable/AnarchyRoad.ogg'

/datum/turntable_soundtrack/blackpoetry
	f_name = "B"
	name = "lack Poetry"
	path = 'sound/turntable/BlackPoetry.ogg'

/datum/turntable_soundtrack/anarchy
	f_name = "A"
	name = "narchy Road"
	path = 'sound/turntable/AnarchyRoad.ogg'

/datum/turntable_soundtrack/callista
	f_name = "C"
	name = "allista"
	path = 'sound/turntable/Callista.ogg'

/datum/turntable_soundtrack/hourglass
	f_name = "H"
	name = "ourglass"
	path = 'sound/turntable/Hourglass.ogg'


/datum/turntable_soundtrack/nfatuation
	f_name = "I"
	name = "nfatuation"
	path = 'sound/turntable/Infatuation.ogg'

/datum/turntable_soundtrack/miamidisco
	f_name = "M"
	name = "iami Disco"
	path = 'sound/turntable/MiamiDisco.ogg'

/datum/turntable_soundtrack/nightmare
	f_name = "N"
	name = "ightmare"
	path = 'sound/turntable/Nightmare.ogg'

/datum/turntable_soundtrack/paradisewarfare
	f_name = "P"
	name = "aradiseWarfare"
	path = 'sound/turntable/ParadiseWarfare.ogg'

/datum/turntable_soundtrack/sally
	f_name = "R"
	name = "un, Sally, Run!"
	path = 'sound/turntable/RunSallyRun.ogg'

/datum/turntable_soundtrack/searching
	f_name = "S"
	name = "earching"
	path = 'sound/turntable/Searching.ogg'

/datum/turntable_soundtrack/maniacula
	f_name = "S"
	name = "he is a Maniac"
	path = 'sound/turntable/SheisAManiac.ogg'

/datum/turntable_soundtrack/electric
	f_name = "S"
	name = "o Electric"
	path = 'sound/turntable/SoElectric.ogg'


/datum/turntable_soundtrack/starhustler
	f_name = "S"
	name = "tar Hustler"
	path = 'sound/turntable/StarHustler.ogg'

/datum/turntable_soundtrack/suffer
	f_name = "S"
	name = "uffer"
	path = 'sound/turntable/Suffer.ogg'

/datum/turntable_soundtrack/thirteen
	f_name = "T"
	name = "hirteen"
	path = 'sound/turntable/Thirteen.ogg'

*/