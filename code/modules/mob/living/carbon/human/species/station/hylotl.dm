/datum/species/hylotl
	name = "Hylotl"
	name_plural = "Hylotl"
	icobase = 'icons/mob/human_races/r_hylotl.dmi'
	deform = 'icons/mob/human_races/r_def_hylotl.dmi'
	//default_language = "Vox-pidgin"
	//language = "Galactic Common"
	unarmed_types = list(/datum/unarmed_attack/bite/strong)
	rarity_value = 2
	blurb = "<err!@#$>"

	//speech_sounds = list('sound/voice/shriek1.ogg')
	//speech_chance = 20

	warning_low_pressure = 60
	hazard_low_pressure = 0

	cold_level_1 = 80
	cold_level_2 = 50
	cold_level_3 = 0

	eyes = "blank_eyes"

	poison_type = "nitrogen"
	siemens_coefficient = 0.2
/*CAN_JOIN | IS_WHITELISTED | */
	flags = CAN_JOIN | IS_WHITELISTED | NO_SCAN | NO_PAIN

	blood_color = "#2266ff"
	flesh_color = "#bbffbb"
