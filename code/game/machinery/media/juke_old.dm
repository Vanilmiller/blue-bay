./sound/turntable/test
	file = 'sound/turntable/TestLoop1.ogg'
	falloff = 2
	repeat = 1

/mob/var/music = 0

/obj/machinery/party/turntable
	name = "Jukebox"
	desc = "A jukebox is a partially automated music-playing device, usually a coin-operated machine, that will play a patron's selection from self-contained media."
	icon = 'icons/obj/jukebox.dmi'
	icon_state = "jukebox2-nopower"
	var/state_base = "jukebox2"
	anchored = 1
	density = 1
	power_channel = EQUIP
	use_power = 1
	idle_power_usage = 10
	active_power_usage = 100
	var/playing = 0
	var/list/songs = list ("Lonely Assistant Blues"='sound/turntable/AGrainOfSandInSandwich.ogg',
		"Chinatown"='sound/turntable/chinatown.ogg',
		"Blue Theme"='sound/turntable/BlueTheme.ogg',
		"The Assassination of Jesse James"='sound/turntable/TheAssassinationOfJesseJames.ogg',
		"Everyone Has Their Vices"='sound/turntable/EveryoneHasTheirVices.ogg',
		"The Way You Look Tonight"='sound/turntable/TheWayYouLookTonight.ogg',
		"Onizukas Blues"='sound/turntable/OnizukasBlues.ogg',
		"Ragtime Piano"='sound/turntable/TheEntertainer.ogg',
		"It Had To Be You"='sound/turntable/ItHadToBeYou.ogg',
		"Another Day's Work"='sound/turntable/AnotherDaysWork.ogg',
		"Razor Walker"='sound/turntable/RazorWalker.ogg',
		"Mute Beat"='sound/turntable/MuteBeat.ogg',
		"Groovy Times"='sound/turntable/GroovyTime.ogg',
		"That`s All"='sound/turntable/ThatsAll.ogg',
		"Classic Blue Jazz" ='sound/turntable/classicbluejazz.ogg',
		"Homosexualism" ='sound/turntable/anthem.ogg',
		"Come Back Down" ='sound/turntable/comebackdown.ogg',
		"Le Perv" ='sound/turntable/leperv.ogg',
		"Parasite" ='sound/turntable/parasite.ogg',
		"I Was a Teenage Werewolf" ='sound/turntable/IWasATeenageWerewolf.ogg',
		"Jack The Ripper" ='sound/turntable/JackTheRipper.ogg',
		"LineDisasterChicken" ='sound/turntable/LineDisasterChicken.ogg',
		"Massacre Anemone" ='sound/turntable/MassacreAnemone.ogg',
		"The Way I Walk" ='sound/turntable/TheWayIWalk.ogg',
		"Young Men Dead" ='sound/turntable/YoungMenDead.ogg',
		"You On The Run" ='sound/turntable/YouOnTheRun.ogg')

/obj/machinery/party/turntable/power_change()
	if(!powered(power_channel) || !anchored)
		stat |= NOPOWER
	else
		stat &= ~NOPOWER

	if(stat & (NOPOWER|BROKEN) && playing)
		StopPlaying()
	update_icon()

/obj/machinery/party/turntable/update_icon()
	overlays.Cut()
	if(stat & (NOPOWER|BROKEN) || !anchored)
		if(stat & BROKEN)
			icon_state = "[state_base]-broken"
		else
			icon_state = "[state_base]-nopower"
		return
	icon_state = state_base
	if(playing)
		if(emagged)
			overlays += "[state_base]-emagged"
		else

/obj/machinery/party/turntable/interact(mob/user)
	if(stat & (NOPOWER|BROKEN))
		usr << "\The [src] doesn't appear to function."
		return

/obj/machinery/party/turntable/attackby(obj/item/W as obj, mob/user as mob)
	src.add_fingerprint(user)

	if(istype(W, /obj/item/weapon/wrench))
		if(playing)
			StopPlaying()
		user.visible_message("<span class='warning'>[user] has [anchored ? "un" : ""]secured \the [src].</span>", "<span class='notice'>You [anchored ? "un" : ""]secure \the [src].</span>")
		anchored = !anchored
		playsound(src.loc, 'sound/items/Ratchet.ogg', 50, 1)
		power_change()
		update_icon()
		return
	if(istype(W, /obj/item/weapon/card/emag))
		if(!emagged)
			emagged = 1
			StopPlaying()
			visible_message("<span class='danger'>\the [src] makes a fizzling sound.</span>")
			log_and_message_admins("emagged \the [src]")
			update_icon()
			return

	return ..()

/obj/machinery/party/turntable/proc/StopPlaying()
	var/area/A = get_area(src)
	// Always kill the current sound
	for(var/mob/living/M in mobs_in_area(A))
		M << sound(null, channel = 1)

	A.forced_ambience = null
	playing = 0
	update_use_power(1)
	update_icon()

/obj/machinery/party/turntable/proc/explode()
	walk_to(src,0)
	src.visible_message("<span class='danger'>\the [src] blows apart!</span>", 1)

	explosion(src.loc, 0, 0, 1, rand(1,2), 1)

	var/datum/effect/effect/system/spark_spread/s = new /datum/effect/effect/system/spark_spread
	s.set_up(3, 1, src)
	s.start()

/obj/machinery/party/turntable/New()
	..()
	sleep(2)
	new /sound/turntable/test(src)
	return

/obj/machinery/party/turntable/attack_hand(mob/living/user as mob)
	if (..())
		return
	usr.set_machine(src)
	src.add_fingerprint(usr)

	var/t = "<body background=turntable.png ><br><br><br><br><br><br><br><br><br><br><br><br><div align='center'>"
	t += "<A href='?src=\ref[src];off=1'><font color='maroon'>T</font><font color='geen'>urn</font> <font color='red'>Off</font></A>"
	t += "<table border='0' height='25' width='300'><tr>"

	for (var/i = 1, i<=(songs.len), i++)
		var/check = i%2
		t += "<td><A href='?src=\ref[src];on=[i]'><font color='maroon'>[copytext(songs[i],1,2)]</font><font color='purple'>[copytext(songs[i],2)]</font></A></td>"
		if(!check) t += "</tr><tr>"

	t += "</tr></table></div></body>"
	user << browse(t, "window=turntable;size=500x636;can_resize=0")
	onclose(user, "urntable")
	return

/obj/machinery/party/turntable/Topic(href, href_list)
	..()
	if( href_list["on"])
		if(src.playing == 0)
			//world << "Should be working..."
			var/sound/S
			S = sound(songs[songs[text2num(href_list["on"])]])
			S.repeat = 1
			S.channel = 10
			S.falloff = 2
			S.wait = 1
			S.environment = 0

			var/area/A = src.loc.loc:master

			for(var/area/RA in A.related)
				for(var/obj/machinery/party/lasermachine/L in RA)
					L.turnon()
			playing = 1
			while(playing == 1)
				for(var/mob/M in world)
					var/area/location = get_area(M)
					if((location in A.related) && M.music == 0)
						//world << "Found the song..."
						M << S
						M.music = 1
					else if(!(location in A.related) && M.music == 1)
						var/sound/Soff = sound(null)
						Soff.channel = 10
						M << Soff
						M.music = 0
				sleep(10)
			return

	if( href_list["off"] )
		if(src.playing == 1)
			var/sound/S = sound(null)
			S.channel = 10
			S.wait = 1
			for(var/mob/M in world)
				M << S
				M.music = 0
			playing = 0
			var/area/A = src.loc.loc:master
			for(var/area/RA in A.related)
				for(var/obj/machinery/party/lasermachine/L in RA)
					L.turnoff()

/obj/machinery/party/lasermachine
	name = "laser machine"
	desc = "A laser machine that shoots lasers."
	icon_state = "lasermachine"
	anchored = 1
	var/mirrored = 0

/obj/effects/laser
	name = "laser"
	desc = "A laser..."
	icon_state = "laserred1"
	anchored = 1
	layer = 4

/obj/item/lasermachine/New()
	..()

/obj/machinery/party/lasermachine/proc/turnon()
	var/wall = 0
	var/cycle = 1
	var/area/A = get_area(src)
	var/X = 1
	var/Y = 0
	if(mirrored == 0)
		while(wall == 0)
			if(cycle == 1)
				var/obj/effects/laser/F = new/obj/effects/laser(src)
				F.x = src.x+X
				F.y = src.y+Y
				F.z = src.z
				F.icon_state = "laserred1"
				var/area/AA = get_area(F)
				var/turf/T = get_turf(F)
				if(T.density == 1 || AA.name != A.name)
					del(F)
					return
				cycle++
				if(cycle > 3)
					cycle = 1
				X++
			if(cycle == 2)
				var/obj/effects/laser/F = new/obj/effects/laser(src)
				F.x = src.x+X
				F.y = src.y+Y
				F.z = src.z
				F.icon_state = "laserred2"
				var/area/AA = get_area(F)
				var/turf/T = get_turf(F)
				if(T.density == 1 || AA.name != A.name)
					del(F)
					return
				cycle++
				if(cycle > 3)
					cycle = 1
				Y++
			if(cycle == 3)
				var/obj/effects/laser/F = new/obj/effects/laser(src)
				F.x = src.x+X
				F.y = src.y+Y
				F.z = src.z
				F.icon_state = "laserred3"
				var/area/AA = get_area(F)
				var/turf/T = get_turf(F)
				if(T.density == 1 || AA.name != A.name)
					del(F)
					return
				cycle++
				if(cycle > 3)
					cycle = 1
				X++
	if(mirrored == 1)
		while(wall == 0)
			if(cycle == 1)
				var/obj/effects/laser/F = new/obj/effects/laser(src)
				F.x = src.x+X
				F.y = src.y-Y
				F.z = src.z
				F.icon_state = "laserred1m"
				var/area/AA = get_area(F)
				var/turf/T = get_turf(F)
				if(T.density == 1 || AA.name != A.name)
					del(F)
					return
				cycle++
				if(cycle > 3)
					cycle = 1
				Y++
			if(cycle == 2)
				var/obj/effects/laser/F = new/obj/effects/laser(src)
				F.x = src.x+X
				F.y = src.y-Y
				F.z = src.z
				F.icon_state = "laserred2m"
				var/area/AA = get_area(F)
				var/turf/T = get_turf(F)
				if(T.density == 1 || AA.name != A.name)
					del(F)
					return
				cycle++
				if(cycle > 3)
					cycle = 1
				X++
			if(cycle == 3)
				var/obj/effects/laser/F = new/obj/effects/laser(src)
				F.x = src.x+X
				F.y = src.y-Y
				F.z = src.z
				F.icon_state = "laserred3m"
				var/area/AA = get_area(F)
				var/turf/T = get_turf(F)
				if(T.density == 1 || AA.name != A.name)
					del(F)
					return
				cycle++
				if(cycle > 3)
					cycle = 1
				X++


/obj/machinery/party/lasermachine/proc/turnoff()
	var/area/A = src.loc.loc
	for(var/area/RA in A.related)
		for(var/obj/effects/laser/F in RA)
			del(F)
